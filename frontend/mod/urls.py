from django.conf.urls import patterns, url

from mod import views

urlpatterns = patterns('',
	url(r'^$', views.index, name='index'),
	url(r'^success/$', views.success, name='success'),
	url(r'^list/$', views.list, name='list'),
)