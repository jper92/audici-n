# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mod', '0002_receiver'),
    ]

    operations = [
        migrations.AddField(
            model_name='receiver',
            name='receiver',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
