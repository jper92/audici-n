# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mod', '0008_auto_20141209_0417'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='receivers',
            field=models.IntegerField(default=0),
            preserve_default=True,
        ),
    ]
