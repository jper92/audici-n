# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mod', '0004_auto_20141202_0432'),
    ]

    operations = [
        migrations.RenameField(
            model_name='receiver',
            old_name='received',
            new_name='enqueued',
        ),
    ]
