# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mod', '0003_receiver_receiver'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='receiver',
            name='receiver',
        ),
        migrations.AddField(
            model_name='receiver',
            name='received',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='message',
            name='sent_at',
            field=models.DateField(null=True),
            preserve_default=True,
        ),
    ]
