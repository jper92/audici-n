# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mod', '0006_auto_20141207_1951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receiver',
            name='enqueued',
            field=models.BooleanField(db_index=True, default=False),
            preserve_default=True,
        ),
    ]
