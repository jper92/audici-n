# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mod.models


class Migration(migrations.Migration):

    dependencies = [
        ('mod', '0007_auto_20141208_0512'),
    ]

    operations = [
        migrations.AlterField(
            model_name='receiver',
            name='number',
            field=models.BigIntegerField(validators=[mod.models.validate_number]),
            preserve_default=True,
        ),
    ]
