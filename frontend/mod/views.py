import csv
from django.conf import settings
from django.contrib import messages
from django.shortcuts import render,redirect
from django.core.exceptions import ValidationError
from mod import models
#from django.http import HttpResponse
from django.db import transaction
import os
#from django.template import loader

# Create your views here.

def index(request):
	if (request.method=="POST"):
		form = models.MessageForm(request.POST, request.FILES)
		if form.is_valid():
			numbers = request.FILES['file_field']
			path = settings.MEDIA_ROOT + "/tmp.csv"
			handle_file(numbers, path)
			with transaction.atomic():
				message = form.save()
				count = 0
				with open(path, 'rt') as numbers:
					reader = csv.reader(numbers)
					for row in reader:
						count = count+1
						n = models.Receiver(number=row[0], message=message)
						try:
							n.clean_fields()
						except ValidationError:
							messages.error(request, 'El formato de uno o más números en el csv no es correcto. Abortando.')
							return render(request, 'mod/index.html', {'form' : form})
						n.save()
					message.receivers = count
					message.save()
					os.popen('java -jar enqueuer.jar ' + str(message.id));
			return redirect('success')
	else:
		form = models.MessageForm()
	return render(request, 'mod/index.html', {'form' : form})


def success(request):
	return render(request, 'mod/success.html')

def list(request):
	messages = models.Message.objects.all()
	return render(request, 'mod/list.html', {'rows' : messages})

def handle_file(f, path):
	with open(path, 'wb+') as destination:
		for chunk in f.chunks():
			destination.write(chunk)