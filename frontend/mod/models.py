from django.db import models
from django.forms import ModelForm
from django import forms

from django.core.exceptions import ValidationError

def validate_number(value):
	if (not str(value).startswith('502') or not len(str(value))==11):
		raise ValidationError('%s is not a valid number' % value)

# Create your models here.

class Message(models.Model):
	text = models.CharField(max_length=160)
	created_at = models.DateTimeField(auto_now_add=True)
	sent_at = models.DateTimeField(null=True)
	receivers = models.IntegerField(default=0)
	status = models.CharField(max_length=32)

class Receiver(models.Model):
	number = models.BigIntegerField(validators=[validate_number])
	message = models.ForeignKey('Message')
	enqueued = models.BooleanField(default=False, db_index=True)

class MessageForm(ModelForm):
	class Meta:
		model=Message
		fields = ['text']
	file_field = forms.FileField()