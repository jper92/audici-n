import java.net.UnknownHostException;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.BasicConfigurator;


public class Consumer implements MessageListener {

	private long total = 0;
	private int messages = 0;
	
	private MongoHelper helper;
	
	private Connection connection;
	private Session session;
	private Destination destination;
	private MessageConsumer consumer;
	
	
	public static void main(String[] args){
		try {
			new Consumer();
		} catch (JMSException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (UnknownHostException e) {
			e.printStackTrace();
			System.exit(1);
		}
	}
	
	public Consumer() throws JMSException, UnknownHostException{
		BasicConfigurator.configure();
		helper = new MongoHelper();
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
		connection = factory.createConnection();
		connection.start();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		destination = session.createQueue("sms");
		consumer = session.createConsumer(destination);
		consumer.setMessageListener(this);
		
	}

	@Override
	public void onMessage(Message message) {
		long initialTime = System.nanoTime();
		if (message instanceof TextMessage){
			TextMessage txt = (TextMessage)message;
			try {
				helper.save(txt.getText());
				long finalTime = System.nanoTime();
				total += finalTime-initialTime;
				messages++;
				System.out.println("Statistics: " + (total/messages) + " nanoseconds per message");
			} catch (JMSException e) {
				e.printStackTrace();
			}
		}
	}
	
}
