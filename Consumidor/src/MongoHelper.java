import java.net.UnknownHostException;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;


public class MongoHelper {
	
	private MongoClient client;
	private DB db;
	private DBCollection collection;
	
	public MongoHelper() throws UnknownHostException{
		client = new MongoClient("localhost", 27017);
		db = client.getDB("sms");
		collection = db.getCollection("messages");
	}
	
	public void save(String json){
		DBObject object = (DBObject)JSON.parse(json);
		collection.insert(object);
	}

}
