package com.javier.audicion;
import java.sql.SQLException;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.json.simple.JSONObject;

import com.javier.audicion.model.Message;


public class QueueHelper {
	
	private ConnectionFactory factory;
	private Connection connection;
	private Session session;
	private Destination destination;
	private MessageProducer producer;
	
	public void connectToJMS(Encolador encolador) throws JMSException{
		factory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);
		connection = factory.createConnection();
		connection.start();
		session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		destination = session.createQueue("sms");
		producer = session.createProducer(destination);
	}
	
	public String toJson(Message message) throws SQLException{
		JSONObject json = new JSONObject();
		json.put("id", message.messageId);
		json.put("number", message.receiver);
		json.put("message", message.message);
		return json.toJSONString();
	}
	
	public void registerMessage(Message message) throws JMSException, SQLException{
		String json = toJson(message);
		TextMessage textMessage = session.createTextMessage(json);
		producer.send(textMessage);
	}
	
	public void closeAll(){
		try {
			if (producer!=null) producer.close();
			if (session!=null) session.close();
			if (connection!=null) connection.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}

}
