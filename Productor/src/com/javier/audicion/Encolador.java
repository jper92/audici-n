package com.javier.audicion;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.jms.JMSException;

import org.apache.log4j.BasicConfigurator;

import com.javier.audicion.model.Message;



public class Encolador {

	public static final String DB_HOST = "localhost";
	public static final String DB_PORT = "3306";
	public static final String DB_NAME = "audicion";
	public static final String DB_USER = "root";
	public static final String DB_PASSWORD = "";
	
	private Connection connect;
	private int messageId;
	private QueueHelper queueHelper;
	private String message;
	private long initialTime;
	
	public String getMessageBody(){
		return message;
	}
	
	public static void main(String[] args){
		if (args!=null && args.length>0){
			BasicConfigurator.configure();
			int number = Integer.parseInt(args[0]);
			
			Encolador encolador = new Encolador(number);
			encolador.performConnection();
		}
		else{
			System.out.print("No args. Exiting.");
			System.exit(1);
		}
		System.exit(0);
		
	}
	
	public Encolador(int messageId){
		this.messageId = messageId;
		initialTime = System.nanoTime();
	}
	
	public void performConnection(){
		try {
			queueHelper = new QueueHelper();
			queueHelper.connectToJMS(this);
			Class.forName("com.mysql.jdbc.Driver");
			connect = DriverManager
					.getConnection("jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?user=" + DB_USER + "&password=" + DB_PASSWORD);
			PreparedStatement msgStatement = connect.prepareStatement("select * from mod_message where id= ? ");
			msgStatement.setInt(1, messageId);
			ResultSet message = msgStatement.executeQuery();
			if (message.next()) this.message = message.getString("text");
			PreparedStatement statement = connect.prepareStatement("select * from mod_receiver where message_id = ? and enqueued!=true limit 50");
			statement.setInt(1, messageId);
			ResultSet set = statement.executeQuery();
			while (processSet(set)){
				set = statement.executeQuery();
			}
			finish();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(1);
		}
		catch (JMSException e) {
			e.printStackTrace();
			System.exit(1);
		}
		finally{
			closeAll();
		}
		System.exit(0);
	}
	
	
	
	public void finish() throws SQLException{
		PreparedStatement statement = connect.prepareStatement("update mod_message set sent_at= utc_timestamp(), status = ? where id= ?");
		long finalTime = System.nanoTime();
		long difference = finalTime - initialTime;
		statement.setString(1, "Time ellapsed: " + difference + "ns");
		statement.setInt(2, messageId);
		statement.executeUpdate();
	}
	
	public void updateInfo(Message message) throws SQLException{
		PreparedStatement msgStatement = connect.prepareStatement("update mod_receiver set enqueued=true where id= ? ");
		msgStatement.setInt(1, message.messageId);
		msgStatement.executeUpdate();
		
	}
	
	public boolean processSet(ResultSet set) throws SQLException, JMSException{
		boolean hadNext = false;
		while (set.next()){
			hadNext = true;
			System.out.println("id: " + set.getString("id") + ". number: " + 
			set.getString("number") + ". message_id " + set.getInt("message_id")
					+ ". Enqueued " + set.getBoolean("enqueued"));
			Message message = Message.fromSet(this, set);
			queueHelper.registerMessage(message);
			updateInfo(message);
		}
		return hadNext;
	}

	public void closeAll(){
		queueHelper.closeAll();
		try {
			if (connect!=null) connect.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
