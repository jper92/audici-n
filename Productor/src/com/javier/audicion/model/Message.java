package com.javier.audicion.model;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.javier.audicion.Encolador;

public class Message {

	public int messageId;
	public String receiver;
	public String message;
	
	public static Message fromSet(Encolador encolador, ResultSet set) throws SQLException{
		Message message = new Message();
		message.messageId = set.getInt("id");
		message.receiver = set.getString("number");
		message.message = encolador.getMessageBody();
		return message;
	}
	
}
